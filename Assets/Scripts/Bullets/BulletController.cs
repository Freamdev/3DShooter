﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

    public int Damage;
    public int DotDamage;
    public int Duration;

    public float Speed;
    private float AliveTime;

    public GameObject Effect;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        AliveTime += Time.deltaTime;
        if(AliveTime > 10)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Walls" || other.tag == "Player" || other.tag == "Enemy")
        {
            Destroy(gameObject);
        }
        else if(other.tag == "Player shield")
        {
            print("Deffended");
            Destroy(gameObject);
        }
        if(other.tag == "Player")
        {
            ApplyMyselfTo(other.gameObject);
        }
    }

    public virtual void ApplyMyselfTo(GameObject effected)
    {
        effected.GetComponent<PlayerController>().HittedBySpell(Damage);
    }
}
