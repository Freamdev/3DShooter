﻿using UnityEngine;
using System.Collections;

public class BoulderController : BulletController
{

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void ApplyMyselfTo(GameObject effected)
    {
        base.ApplyMyselfTo(effected);
        effected.GetComponent<PlayerController>().HittedInMelee(Damage);
    }
}
