﻿using UnityEngine;
using System.Collections;

public class HealthController : MonoBehaviour {

    public float MaxHealth;
    public float Health;
    [HideInInspector]
    public bool IsAlive = true;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Health <= 0)
        {
            IsAlive = false;
            transform.Rotate(new Vector3(1, 0, 0), 90);
            GetComponent<CharacterController>().enabled = false;
            
            if (gameObject.tag == "Enemy")
            {
                GetComponent<EnemyController>().enabled = false;
                GetComponent<BoxCollider>().enabled = false;
                if (GetComponent<EarthQuake>() != null)
                {
                    GetComponent<EarthQuake>().enabled = false;
                }
                GetComponent<EnemyController>().DropItems();
            }
            enabled = false;
        }
    }
}
