﻿using UnityEngine;
using System.Collections;

/// <summary>
/// https://www.coursera.org/learn/game-development/home/week/4
/// </summary>
public class Controller : MonoBehaviour
{

    // public variables
    [Range(0, 10)]
    public float Speed;
    public float gravity = 9.81f;
    public Transform CamTransform;

    public bool CanMove = true;

    public GameObject Player;
    private CharacterController myController;

    // Use this for initialization
    void Start()
    {
        myController = gameObject.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        CanMove = Player.GetComponent<PlayerController>().CanMove;
        if (CanMove)
        {
            // Determine how much should move in the z-direction
            Vector3 movementZ = Input.GetAxis("Vertical") * Vector3.forward * Speed * Time.deltaTime;
            // Determine how much should move in the x-direction
            Vector3 movementX = Input.GetAxis("Horizontal") * Vector3.right * Speed * Time.deltaTime;
            // Convert combined Vector3 from local space to world space based on the position of the current gameobject (player)
            //Vector3 movement = transform.TransformDirection(movementZ+movementX);
            Vector3 movement = CamTransform.transform.TransformDirection(movementZ + movementX);
            // Apply gravity (so the object will fall if not grounded)
            movement.y -= gravity * Time.deltaTime;
            // Actually move the character controller in the movement direction
            myController.Move(movement);
        }
    }

}
