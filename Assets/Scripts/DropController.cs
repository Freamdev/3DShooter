﻿using UnityEngine;
using System.Collections;

public class DropController : MonoBehaviour {

    [Range(1,360)]
    public float RotateSpeed;

    [Range(0, 360)]
    public float ExtraRotationOnPickUp_X;

    [Range(0, 360)]
    public float ExtraRotationOnPickUp_Y;

    [Range(0, 360)]
    public float ExtraRotationOnPickUp_Z;

    public GameObject TurnIntoThis;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 1, 0), Time.deltaTime * RotateSpeed);
	}
}
