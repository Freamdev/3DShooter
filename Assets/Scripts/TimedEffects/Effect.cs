﻿using UnityEngine;
using System.Collections;

public class Effect : MonoBehaviour {

    public GameObject EffectPrefab;
    public float Length;
    public int Damage;

    public bool OverriteParticleEffectsTime;

    protected float length;

    public virtual void Activate(GameObject effected)
    {
        var effect = Instantiate(EffectPrefab, effected.transform.position, Quaternion.identity) as GameObject;
        effect.transform.parent = effected.transform;
        if (OverriteParticleEffectsTime)
        {
            length = Length;
        }
        else
        {
            length = effect.GetComponent<ParticleSystem>().startLifetime + effect.GetComponent<ParticleSystem>().duration;
        }
    }
}
