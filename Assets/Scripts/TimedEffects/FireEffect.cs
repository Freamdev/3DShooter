﻿using UnityEngine;
using System.Collections;

public class FireEffect : Effect
{



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<ParticleSystem>().isPlaying)
        {
            Destroy(gameObject);
        }
    }

    public override void Activate(GameObject effected)
    {
        base.Activate(effected);
        effected.GetComponent<PlayerController>().SetOnFire(Damage, length);
    }
}
