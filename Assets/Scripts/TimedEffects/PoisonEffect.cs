﻿using UnityEngine;
using System.Collections;

public class PoisonEffect : Effect
{

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!GetComponent<ParticleSystem>().isPlaying)
        {
            Destroy(gameObject);
        }
    }

    public override void Activate(GameObject effected)
    {
        base.Activate(effected);
        effected.GetComponent<PlayerController>().Poison(Damage, length);
    }
}
