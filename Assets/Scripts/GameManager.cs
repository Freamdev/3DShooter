﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectsWithTag("EditorOnly").ToList().ForEach(x => x.SetActive(false));
        GameObject.FindGameObjectsWithTag("Walls").ToList().ForEach(x => x.AddComponent<WallController>());
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}

