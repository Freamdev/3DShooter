﻿using UnityEngine;
using System.Collections;

public class EarthQuake : MonoBehaviour {

    public EnemyController Controller;
    public GameObject Target;

    public GameObject BoulderPrefab;

    public float Cooldown;
    public float TimeOfUse;
    public float Range;

	// Use this for initialization
	void Start () {
        Controller = GetComponent<EnemyController>();

    }
	
	// Update is called once per frame
	void Update () {
        Target = Controller.Player;
        if(Target != null)
        {
            EarthQuakeEffect();
        }
	}

    void EarthQuakeEffect()
    {
        if(Cooldown + TimeOfUse < Time.timeSinceLevelLoad && Vector3.Distance(Target.transform.position, transform.position) < Range)
        {
            
            
            TimeOfUse = Time.timeSinceLevelLoad;
            for(int i = 0; i < 15; i++)
            {
                var pos = new Vector3(Random.Range(-Range, Range), 10, Random.Range(-Range, Range));
                Instantiate(BoulderPrefab, pos + transform.position, Quaternion.identity);
            }
            StartCoroutine(EarthQuakeOver());
        }
    }

    IEnumerator EarthQuakeOver()
    {
        Camera.main.GetComponent<CameraController>().earthQuakeHappening = true; 
        yield return new WaitForSeconds(4);
        Camera.main.GetComponent<CameraController>().earthQuakeHappening = false;
    }
}
