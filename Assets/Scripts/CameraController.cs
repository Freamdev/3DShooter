﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// https://www.youtube.com/watch?v=Ta7v27yySKs
/// </summary>
public class CameraController : MonoBehaviour
{

    public GameObject Target;


    private Dictionary<Transform, Material> _LastTransforms;
    public LayerMask OccluderMask;

    //This is the material with the Transparent/Diffuse With Shadow shader
    public Material HiderMaterial;

    public bool earthQuakeHappening;


    private float distance = -6.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private float sensivetyX = 10.0f;
    private float sensivetyY = 1.0f;
    private Camera Cam;

    public Transform CamTransform;

    // Use this for initialization
    void Awake()
    {
        _LastTransforms = new Dictionary<Transform, Material>();
        if (Target == null)
        {
            Debug.LogWarning("Player prefab not set. Looking for object with player tag.");
            Target = GameObject.FindGameObjectWithTag("Player");
        }
    }

    void Start()
    {
        CamTransform = transform;
        Cam = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            currentX += Input.GetAxis("Mouse X") * sensivetyX;
        }
        transform.LookAt(Target.transform);
        if (Input.GetKey(KeyCode.Q))
        {
            transform.RotateAround(Target.transform.position, new Vector3(0, 1, 0), 5);
            // transform.Rotate(new Vector3(0, 1, -1), 5);
        }

        if (_LastTransforms.Count > 0)
        {
            foreach (Transform t in _LastTransforms.Keys)
            {
                t.GetComponent<Renderer>().material = _LastTransforms[t];
            }
            _LastTransforms.Clear();
        }

        //Cast a ray from this object's transform the the watch target's transform.
        RaycastHit[] hits = Physics.RaycastAll(
            transform.position,
            Target.transform.position - transform.position,
            Vector3.Distance(Target.transform.position, transform.position),
            OccluderMask
        );

        //Loop through all overlapping objects and disable their mesh renderer
        if (hits.Length > 0)
        {
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.gameObject.transform != Target && hit.collider.transform.root != Target)
                {
                    _LastTransforms.Add(hit.collider.gameObject.transform, hit.collider.gameObject.GetComponent<Renderer>().material);
                    hit.collider.gameObject.GetComponent<Renderer>().material = HiderMaterial;
                }
            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //transform.position = Target.transform.position + Offset;

        Vector3 dir = new Vector3(0, 3, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        if (earthQuakeHappening)
        {
            rotation *= Quaternion.Euler(Random.Range(0, 10f), 0, Random.Range(0, 10f));
        }
        CamTransform.position = Target.transform.position + rotation * dir;
        CamTransform.LookAt(Target.transform.position);
    }
}
