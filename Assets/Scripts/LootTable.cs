﻿using UnityEngine;
using System.Collections;

public class LootTable : MonoBehaviour {

    [System.Serializable]
    public class Loot
    {
        public string Name;
        public GameObject item;
        public float chance;
    }

    public Loot[] Loots;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
