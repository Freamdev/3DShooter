﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    #region Inspector Classes
    [System.Serializable]
    public class Ranged
    {
        public bool isRanged;
        public GameObject RangedAttack;
        public float CooldownOnRangedAttack;
        [Range(0, 25)]
        public float ApproachRadious;
        public float TimeOfFire;
    }

    [System.Serializable]
    public class Melee
    {
        public float CooldownOnMeleeStrike;
        public float TimeOfHit;
        public GameObject Effect;
        public int Damage;
    }

    #endregion

    [Range(0, 10)]
    public float Speed;

    [Range(0, 10)]
    public float TurnRate;

    [Range(0, 25)]
    public float ChaseRadious;

    public float ExpWorth;

    public Ranged RangedAttributes;
    public Melee MeleeAttributes;

    public GameObject Player;


    private CharacterController MyController;

    [HideInInspector]
    public HealthController MyHealthAttributes;

    [HideInInspector]
    public LootTable LootTable;



    // Use this for initialization
    void Awake()
    {
        if (Player == null)
        {
            Debug.LogWarning("Player prefab not set. Looking for object with player tag.");
            Player = GameObject.FindGameObjectWithTag("Player");
        }


        MyController = gameObject.GetComponent<CharacterController>();
        MyHealthAttributes = gameObject.GetComponent<HealthController>();
        LootTable = gameObject.GetComponent<LootTable>();
    }

    // Update is called once per frame
    void Update()
    {
        ChaseAndAttack();
    }

    void ChaseAndAttack()
    {
        if (Vector3.Distance(Player.transform.position, transform.position) < ChaseRadious)
        {

            LookAtPlayer();
            if (RangedAttributes.isRanged)
            {
                if (Vector3.Distance(Player.transform.position, transform.position) > RangedAttributes.ApproachRadious)
                {
                    MoveTowardsPlayer();
                }
                else
                {
                    ShootAtPlayer();
                }
            }
            else
            {
                MoveTowardsPlayer();
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, ChaseRadious);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, RangedAttributes.ApproachRadious);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, Speed);
    }

    void LookAtPlayer()
    {
        var dir = (Player.transform.position - transform.position).normalized;
        var lookDir = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookDir, TurnRate * Time.deltaTime);
    }

    void ShootAtPlayer()
    {
        if (RangedAttributes.CooldownOnRangedAttack + RangedAttributes.TimeOfFire < Time.timeSinceLevelLoad)
        {
            RangedAttributes.TimeOfFire = Time.timeSinceLevelLoad;
            var dir = Player.transform.position;
            var spell = Instantiate(RangedAttributes.RangedAttack, transform.position, transform.rotation) as GameObject;
            spell.GetComponent<Rigidbody>().AddForce(spell.transform.forward * spell.GetComponent<BulletController>().Speed, ForceMode.VelocityChange);
        }
    }

    void MoveTowardsPlayer()
    {
        Vector3 movementZ = Vector3.forward * Speed * Time.deltaTime;
        Vector3 movement = transform.TransformDirection(movementZ);
        movement.y -= 9.81f * Time.deltaTime;
        MyController.Move(movement);
    }

    public void MeleeHittedPlayer(PlayerController playerController)
    {
        if (MeleeAttributes.CooldownOnMeleeStrike + MeleeAttributes.TimeOfHit < Time.timeSinceLevelLoad)
        {
            MeleeAttributes.TimeOfHit = Time.timeSinceLevelLoad;
            playerController.HittedInMelee(MeleeAttributes.Damage);
            if(MeleeAttributes.Effect != null) { 
                MeleeAttributes.Effect.GetComponent<Effect>().Activate(playerController.gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player weapon")
        {
            gameObject.GetComponent<ParticleSystem>().Play(false);
            var attackerController = other.transform.parent.gameObject.GetComponent<PlayerController>();
            float dmg = (attackerController.CharacterPoints.Str / 7) + attackerController.wepController.Damage;
            MyHealthAttributes.Health -= dmg;
        }
    }

    public void DropItems()
    {
        if (LootTable.Loots.Length > 0)
        {
            foreach (var loot in LootTable.Loots)
            {
                float chance = Random.Range(0, 100);
                if (loot.chance > chance)
                {
                    Instantiate(loot.item, transform.position, Quaternion.identity);
                }
            }
        }
        Player.GetComponent<PlayerController>().AddExp(ExpWorth);
    }
}

