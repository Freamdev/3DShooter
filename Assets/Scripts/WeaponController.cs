﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

    [Range(0,100)]
    public float TurnRate;

    [Range(0, 100)]
    public float Damage;

    public GameObject TurnIntoThis;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
