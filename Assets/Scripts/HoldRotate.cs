﻿using UnityEngine;
using System.Collections;

public class HoldRotate : MonoBehaviour {

    Quaternion originalRotation;

	// Use this for initialization
	void Start () {
        transform.Rotate(new Vector3(1, 0, 0), 270);
        originalRotation = transform.rotation;
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void LateUpdate()
    {
        transform.rotation = originalRotation;
    }
}
